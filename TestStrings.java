import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;



public class TestStrings {
    public static void main(String[] args) {
		
		StringBuilder fileName = new StringBuilder("example.doc");
		fileName.replace(fileName.length()-3, fileName.length(), "bak");

		System.out.println(fileName);

		
		String name1 = "Fabuola";
		String name2 = "Pierre";

		if (name1.equals(name2))
        System.out.println("Wow, identical!");

        String greater = (name1.compareTo(name2) > 0) ? name1 : name2;
        System.out.println("The greater String is " + greater);

		
		String text = "the quick brown fox swallowed down the lazy chicken";

		int count = 0;
		for (int i=0; i<text.length()-2; i++)
		{
			if (text.substring(i, i+2).equals("ow"))
				count++;
		}
		System.out.printf("the text ow appears %d times and", count);

		
		StringBuilder pali = new StringBuilder("Livenotonevil");
		StringBuilder paliCopy = new StringBuilder(pali);
		if (pali.reverse().toString().equalsIgnoreCase(paliCopy.toString()))
			System.out.println("it's a palindrome");

		//part2
        Format year = new SimpleDateFormat("yyyy");
        
        Format dayMonthYear = new SimpleDateFormat("dd MM yyyy");
        
        Format time = new SimpleDateFormat("hh mm");
        
		System.out.println(year.format(new Date()));
		System.out.println(dayMonthYear.format(new Date()));
		System.out.println(time.format(new Date()));


	}

}