public class SavingsAccount extends Account {
    public SavingsAccount(String s, double d) {
        super (s,d);
    }

    @Override
    public void addInterest(){
        setBalance(getBalance()*1.4);
    }
}
