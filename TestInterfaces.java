public class TestInterfaces 
{
    
    public static void main(String[] args)
    {
        Detailable[] details= {
            new HomeInsurance(9,91,911),
            new SavingsAccount("Fabuola",26), //error:type mismatch:cannot convert from SavingsAccount to Detailable??
            new CurrentAccount("Pierre", 98) //same error as above
        };
        for (int i=0; i<details.length; i++)
        {
            System.out.println(details[i].getDetails());
        }
    }
}