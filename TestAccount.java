public class TestAccount {

    public static void main(String[] args){
/*
    Account myAccount = new Account();

    //variables
    myAccount.setName("Fabuola Pierre");
    myAccount.setBalance(54982);

    //print out name and balance
    System.out.println("The name on the account is " + myAccount.getName());
    System.out.println("The balance on the account is " + myAccount.getBalance());
    
    myAccount.addInterest();
    System.out.println("The balance on the account is now " + myAccount.getBalance());
*/

    Account[] arrayOfAccounts;

    arrayOfAccounts = new Account[5];

    double[] amounts = {23,5444,2,345,34};
    String[] names = {"Picard","Ryker", "Worf","Troy","Data"};

    
    for (int i=0; i<arrayOfAccounts.length; i++)
    {
        arrayOfAccounts[i] = new Account();

        arrayOfAccounts[i]. setName(names[i]);
        arrayOfAccounts[i]. setBalance(amounts[i]);

        System.out.println("The name is " + arrayOfAccounts[i].getName());
        System.out.println("The balance is " + arrayOfAccounts[i].getBalance());
        
        arrayOfAccounts[i].addInterest();
        System.out.println("The new balance is " +arrayOfAccounts[i].getBalance());

    }
}

    }