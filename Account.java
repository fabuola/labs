public class Account {
 
    //properties
    private String name;
    private double balance;

    private static double interestRate=0.9 ;

    //lab 8
    public Account(String s, double d) {
        name = s;
        balance = d;

    }

    public Account()
    {
        this("Fabuola", 50);
    }
        

    //constructor
    //lab 6
    //methods
    public void setBalance(double d)
    {
        balance = d;
    }
    
    public double getBalance()
    {
        return balance;
    }

    public String getName()
    {
        return name;
    }
    
    public void setName(String s)
    {
        name = s;
    }

    //lab 11
    public void addInterest()
    {
        //balance = balance *1.1;
    }
    

    // method overloading
	public boolean withdraw()
	{
		return withdraw(20);
	}

	public boolean withdraw(double amount)
	{
		boolean flag = false;
		if ((balance-amount) > 0)
		{
			balance -= amount;
			flag = true;
			System.out.println("You have successfully withdrawn " + amount +".");
			System.out.println("There is now a balance of " + balance);
		}
		else
			System.out.println("Insuffiecient funds. You only have " + balance);
		return flag;
	}


	// static methods
	public static void setInterestRate(double d)
	{
		interestRate = d;
	}

	public static double getInterestRate()
	{
		return interestRate;
	}





}
