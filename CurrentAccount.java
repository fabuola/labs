public class CurrentAccount extends Account {
    public CurrentAccount(String s, double d) {
        super (s,d);
    }

    @Override
    public void addInterest(){
        setBalance(getBalance()*1.1);
    }
}