public class TestInheritance {
    public static void main(String[] args)
	{
		Account[] accounts= {
			new Account("Brigitte", 2),
			new SavingsAccount("Henry", 4),
			new CurrentAccount("Frank", 6)
    };

		for (int i=0; i<accounts.length; i++)
		{
			accounts[i].addInterest();
			System.out.println("The name is " + accounts[i].getName());
			System.out.println("The balance is " + accounts[i].getBalance());
		}
	}
}